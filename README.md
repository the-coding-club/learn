The webpage can be viewed at [https://the-coding-club.gitlab.io/learn/](https://the-coding-club.gitlab.io/learn/).

# The Learn Project

The "Learn" project is focused on making it easy for participants of the coding club improve their skills and find content that helps them with their learning. Our initial focus will be on making it easy for new comers to develop the skills needed to join a project team. Of course we can keep adding content as need be that will also benefit project teams.

Note that we should not be re-inventing the wheel, so when there is content already available, we should link to it. Of course we should not limit ourselves either, so if the team thinks they can "learn" by re-implementing any content/tools that is great as well.

## Beginners outline

As said, the aim for the beginners is to move as soon as possible to a project team since this is where they can learn even more plus get their hands dirty. Of course, participants are free to move back to the "beginners" group if they feel like they need more time since this should make it easier for beginners to try as soon as possible to move to a project team. Furthermore, this project could be a great project for beginners especially if they see something that they can improve.

The rough outline is

- Web Development with HTML, CSS and Javascript
- Exercise: Creating of an "About Me" page
- Git Basics since this is how they will collaborate with other members
- Exercise: Creating a merge request to the about project with the about me page

After this they could be seen to have "graduated" the beginners course and encouraged to join a project team.

We should also introduce useful tools like:

- Text Editors (Atom for example)
- GUI for git (Sourcetree for example)
- Browser debugging tools (Chrome's developer tools for example)
- Workflow automation (gulp for example)

We should also point to interesting tutorials/exercises which could be useful next steps for exampl:

- Tutorial how to deploy to Heroku
- Creating a simple web application with a data store
- Tutorial to create web games
- Tutorial to create music with code

## Technical Details of this Website

We are using [gulp](http://gulpjs.com/) for workflow automation and [nunjucks](https://mozilla.github.io/nunjucks/) as the template engine.

### Building the Website

One needs to have the following installed:

- [node](https://nodejs.org/en/download/): Runtime engine for javascript. This means that you can write code in javascript and have it execute outside of the browser. This will allow you to program in javascript both on the client-side (the browser) and server-side. We will be using node for Workflow automation, and if time permits we will also have some serverside code running in the cloud.
- [gulp](http://gulpjs.com/): Used to automate workflows. Gulp is written in javascript and runs on the node runtime engine. To install enter "npm install gulp-cli -g" in your command line/prompt. This will install gulp and make it available.
- [bower](https://bower.io/): Used to download web dependencies (mainly javascript and CSS). To install enter "npm install bower -g" in your command line/prompt. This will install bower and make it availabl.

In the project directory execute:

- ´npm install´
- ´bower install´
- ´gulp´
