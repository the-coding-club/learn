import pytest
from time import sleep
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC

class TestLearn():
    """Use a test class for tests grouping"""
    
    def setup_class(self):
	"""Setup test suite"""
        #Chrome driver is used here
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)

    def teardown_class(self):
        """Destruct test suite"""
        self.driver.quit()

    def test_case_1(self):
        """Test main page is accessible"""
        try:
            self.driver.get('https://the-coding-club.gitlab.io/learn/')
            WebDriverWait(self.driver, 1).until(EC.title_contains("The Coding Club"))
        except NoSuchElementException as ex:
            pytest.fail("Page was opened but title seems to be wrong." + ex.msg)
        except TimeoutException as ex:
            pytest.fail("Main page failed to be loaded. " + ex.msg)

    def test_case_2(self):
        """Test that header is present and correct"""
        el = self.driver.find_element_by_tag_name('h2')
        assert "The Curriculum" in el.text

    def test_case_3(self):
        """Stub with sleep"""
        sleep(5)
        pass
