window.addEventListener("load", function (){
  var editorNodes=document.querySelectorAll("[data-html-css-editor]");
  function init(editorElement){
    var iframe=editorElement.querySelector("iframe");
    var styleElement=iframe.contentDocument.createElement('style');
    var codeHtmlElement=editorElement.querySelector("code.html");
    var codeCssElement=editorElement.querySelector("code.css");
    function updateCss(){
      styleElement.innerHTML=codeCssElement.textContent;
    }
    function updateHtml(){
      iframe.contentDocument.body.innerHTML=codeHtmlElement.textContent;
    }
    codeCssElement.addEventListener("keyup", updateCss, false);
    codeHtmlElement.addEventListener("keyup", updateHtml, false);
    iframe.contentDocument.head.appendChild(styleElement);
    updateCss();
    updateHtml();
  }

  for (var i = 0, len = editorNodes.length; i < len; i++) {
    init(editorNodes[i]);
  };
},false);
