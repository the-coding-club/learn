var gulp = require('gulp'),
  server = require('gulp-server-livereload'),
  sass = require('gulp-sass'),
  nunjucksRender = require('gulp-nunjucks-render'),
  fm = require('front-matter'),
  notifier = require('node-notifier')
  notify = require('gulp-notify'),
  gutil = require('gulp-util'),
  data = require('gulp-data'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  mainBowerFiles = require('main-bower-files'),
  cheerio = require('cheerio'),
  viz = require("viz.js"),
  through2 = require("through2");

gulp.task('pages',function(){
  return gulp.src(['pages/**/*.html'])
  .pipe(data(function(file) {
      var content = fm(String(file.contents));
      file.contents = new Buffer(content.body);
      return content.attributes;
  }))
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: ['templates']
    }))
  // Render Dot diagrams using viz.js
  .pipe(through2.obj(function(file, encoding, callback) {
      const $ = cheerio.load(file.contents)
      $("[data-dot]").each(function(index,element){
        const $e=$(element);
        if($e.attr('data-dot-images')){
          $e.html(viz($e.text(),{images:$e.data('dot-images')}));
        }else{
          $e.html(viz($e.text()));
        }
        // Cleanup after viz, since it adds a listener on the process and forgets to remove it.
        process.removeListener('uncaughtException',process.listeners('uncaughtException')[process.listeners('uncaughtException').length-1]);
      });
      file.contents=new Buffer($.html());
      callback(null, file);
    }))
  // output files in build folder
  .pipe(gulp.dest('build'))
});

gulp.task('pages-style',function(){
  return gulp.src('pages/css/style.scss')
  .pipe(sass({outputStyle: 'compressed'}).on('error', function(error){
    var message = new gutil.PluginError('sass', error.messageFormatted).toString();
    notifier.notify({
      'title':'There is an error in the styles',
      'message':error.message
    });
    process.stderr.write(message + '\n');
    this.emit('end');
  }))
  .pipe(gulp.dest('build/css'));
});

gulp.task('pages-imgs',function(){
  return gulp.src(['templates/imgs/**/*','pages/imgs/**/*'])
  // output files in build folder
  .pipe(gulp.dest('build/imgs'))
});

gulp.task('js',function(){
  return gulp.src(["js/**/*.js"])
  .pipe(uglify())
  .pipe(gulp.dest('build/lib'))
});

gulp.task('vendor',function(){
  return gulp.src(mainBowerFiles({
    "overrides":{
      "reveal.js":{
        "main":[
          "js/reveal.js",
          "css/reveal.css",
          "plugin/markdown/marked.js",
          "plugin/markdown/markdown.js",
          "plugin/notes/notes.js",
          "plugin/highlight/highlight.js",
          "plugin/math/math.js",
          "css/theme/white.css",
          "lib/font/source-sans-pro/**",
          "lib/css/zenburn.css"
        ]
      }
    }
  }),{ base: 'bower_components' })
  .pipe(gulp.dest('build/lib'));
});


gulp.task('build',['pages','pages-style','pages-imgs','js','vendor']);

gulp.task('watch', function() {
  gulp.watch(['index.html','templates/**/*.html','pages/**/*.html'], ['pages']);
  gulp.watch(['style.scss'], ['pages-style']);
  gulp.watch(["js/**/*.js"], ['js']);
  gulp.watch(['templates/imgs/**/*','pages/imgs/**/*'],['pages-imgs']);
})

gulp.task('serve', ['build'], function() {
  gulp.src('build')
    .pipe(server({
      livereload: true,
      directoryListing: false,
      open: true
    }));
});

gulp.task('default',['build','serve','watch']);
